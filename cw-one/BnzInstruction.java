package cw1;

/**
 * This class ....
 *
 * @author someone
 */

public class BnzInstruction extends Instruction {

    private int register;
    private String label;


    public BnzInstruction(String label, String op) {
        super(label, op);
    }

    public BnzInstruction(String label, int register, String newLabel) {
        this(label, "bnz");
        this.register = register;
        this.label = newLabel;
    }

    @Override
    public void execute(Machine m) {
        if(m.getRegisters().getRegister(register) != 0){
            m.setPc(m.getLabels().indexOf(label));
        }
    }

    @Override
    public String toString() {
        return super.toString() + " register " + register + " to Label "  + label;
    }
}
